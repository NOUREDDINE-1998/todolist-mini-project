package com.noureddine.todolist.todolist;

import datamodel.ToDOItem;
import datamodel.ToDoData;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class Controller {

    private List<ToDOItem> todoItems;
        @FXML
        private ListView<ToDOItem> toDoListView;
        @FXML
        private TextArea itemDetailsTextArea;
        @FXML
        private Label deadLineLabel;

        @FXML
        private BorderPane mainBorderPane;

        @FXML
        public ContextMenu listContextMenu;

        @FXML
        ToggleButton filterToggleButton;

        @FXML
        MenuItem exit;

        private FilteredList<ToDOItem> filteredList ;

        private Predicate<ToDOItem> wantALLItems;
        private Predicate<ToDOItem> wantTodayItems;



      @FXML
    public void closeApp(ActionEvent event){
          if(event.getSource().equals(exit)){
              Platform.exit();
          }
    }


    public void initialize(){
        listContextMenu= new ContextMenu();
        MenuItem deleteMenuItem= new MenuItem("Delete");
        deleteMenuItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ToDOItem item= toDoListView.getSelectionModel().getSelectedItem();
                deleteItem(item);
            }
        });

        listContextMenu.getItems().addAll(deleteMenuItem);
        toDoListView.setContextMenu(listContextMenu);

        toDoListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ToDOItem>() {
            @Override
            public void changed(ObservableValue<? extends ToDOItem> observableValue, ToDOItem toDOItem, ToDOItem t1) {
                DateTimeFormatter formatter= DateTimeFormatter.ofPattern("MMMM d,yyyy");
                if(t1!=null){
                    ToDOItem item= toDoListView.getSelectionModel().getSelectedItem();
                    itemDetailsTextArea.setText(item.getDetails().trim());
                    deadLineLabel.setText(formatter.format(item.getDeadLine()) );
                     //deadLineLabel.setText(item.getDeadLine().toString());

                }
            }
        });


        wantALLItems= new Predicate<ToDOItem>() {
            @Override
            public boolean test(ToDOItem item) {

                return true;
            }
        };

        wantTodayItems= new Predicate<ToDOItem>() {
            @Override
            public boolean test(ToDOItem item) {
                return (item.getDeadLine().equals(LocalDate.now()));
            }
        };

        filteredList=  new FilteredList<ToDOItem>(ToDoData.getInstance().getTodoItems(),wantALLItems);

        SortedList<ToDOItem> sortedList= new SortedList<ToDOItem>(filteredList, new Comparator<ToDOItem>() {
            @Override
            public int compare(ToDOItem o1, ToDOItem o2) {
                return o1.getDeadLine().compareTo(o2.getDeadLine());
            }
        });

        toDoListView.setItems(sortedList);
        toDoListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        toDoListView.getSelectionModel().selectFirst();
    }

    @FXML
    public void showNewItemDialog() throws IOException {
        Dialog<ButtonType> dialog= new Dialog<>();
       dialog.initOwner(mainBorderPane.getScene().getWindow());
        dialog.setTitle("Add New To Do Item");
        FXMLLoader fxmlLoader= new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("toDoItemDialog.fxml"));
        try{
            //Parent root= FXMLLoader.load(getClass().getResource("toDoItemDialog.fxml"));
            dialog.getDialogPane().setContent(fxmlLoader.load());
        }catch (Exception e){
            System.out.println("couldn't load the dialog");
            e.printStackTrace();
            return;

        }
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        Optional<ButtonType> result= dialog.showAndWait();
        if(result.isPresent()&& result.get()==ButtonType.OK){
            DialogController controller= fxmlLoader.getController();
            ToDOItem item= controller.processResults();
            toDoListView.getSelectionModel().select(item);

        }

    }


    @FXML
    public void keyPressed(KeyEvent keyEvent){
        ToDOItem item= toDoListView.getSelectionModel().getSelectedItem();
        if(item!=null){
            if(keyEvent.getCode().equals(KeyCode.DELETE)){
                deleteItem(item);
            }
        }
    }


    @FXML
    public void handleClickListView(){
        ToDOItem item= toDoListView.getSelectionModel().getSelectedItem();
        itemDetailsTextArea.setText(item.getDetails());
        deadLineLabel.setText(item.getDeadLine().toString());
        StringBuilder sb= new StringBuilder(item.getDetails());
        sb.append("\n\n\n\n");
        sb.append("due: "+ item.getDeadLine().toString());
        itemDetailsTextArea.setText(sb.toString());
    }

    public void deleteItem(ToDOItem item){
        Alert alert= new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete todo item");
        alert.setHeaderText("Delete item:"+item.getShortDescription());
        alert.setContentText("Are you sure? press ok to confirm or cancel to back out.");
        Optional<ButtonType> result= alert.showAndWait();
        if(result.isPresent()&& result.get()==ButtonType.OK){
            ToDoData.getInstance().deleteItem(item);
        }

    };

        @FXML
    public void handleToggleFilterButton(){
            ToDOItem selectedItem= toDoListView.getSelectionModel().getSelectedItem();
        if(filterToggleButton.isSelected()){
            filteredList.setPredicate(wantTodayItems);
            if(filteredList.isEmpty()){
                    itemDetailsTextArea.clear();
                    deadLineLabel.setText("");

            }
            else if(filteredList.contains(selectedItem)){
                toDoListView.getSelectionModel().select(selectedItem);
            }else{
                toDoListView.getSelectionModel().selectFirst();
            }
        }else {
            filteredList.setPredicate(wantALLItems);
            toDoListView.getSelectionModel().select(selectedItem);
        }
    }


}