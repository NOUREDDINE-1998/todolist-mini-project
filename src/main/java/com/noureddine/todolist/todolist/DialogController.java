package com.noureddine.todolist.todolist;

import datamodel.ToDOItem;
import datamodel.ToDoData;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.time.LocalDate;

public class DialogController {
    @FXML
    public TextField shorDescriptionField;
    @FXML
    public TextArea detailsArea;
    @FXML
    public DatePicker deadLinePicker;
@FXML
    public ToDOItem processResults(){
        String shortDescription=shorDescriptionField.getText();
        String details= detailsArea.getText();
        LocalDate deadLine=deadLinePicker.getValue();
        ToDOItem item= new ToDOItem(shortDescription,details,deadLine);
        ToDoData.getInstance().addItem(item);
        return item;
    }



}
