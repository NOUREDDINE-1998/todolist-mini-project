package com.noureddine.todolist.todolist;

import datamodel.ToDoData;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("mainWindow.fxml"));
        stage.setTitle("ToDo List!");
        stage.setScene(new Scene(root,900,500));
        stage.setResizable(false);
        stage.show();
        Task<String> t= new Test();
        System.out.println(t);
        (new Thread()).start();
    }

    public static void main(String[] args) {

        launch();


    }

    class Test extends Task{

        @Override
        protected String call() throws Exception {
            return "hello";
        }
    }

    @Override
    public void stop() throws Exception {
        try{
            ToDoData.getInstance().storeToDoItem();
        }catch (Exception e){
            System.out.println(e.getMessage());

        }
    }

    @Override
    public void init() throws Exception {
        try {
            ToDoData.getInstance().loadToDoItems();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}