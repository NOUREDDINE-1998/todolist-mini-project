package datamodel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

public class ToDoData {
    private static ToDoData instance= new ToDoData();
    private static String filename="ToDoListItems.txt";

    private ObservableList<ToDOItem> todoItems;
    private DateTimeFormatter formatter;

    public static ToDoData getInstance(){
        return instance;
    }

    private ToDoData(){
        formatter= DateTimeFormatter.ofPattern("dd-MM-yyyy");
    }

    public ObservableList<ToDOItem> getTodoItems() {
        return todoItems;
    }
    public void addItem(ToDOItem item){
        todoItems.add(item);
    }


    public void loadToDoItems() throws IOException {
        todoItems= FXCollections.observableArrayList();
        //Path path= Paths.get(filename);
        //BufferedReader br = Files.newBufferedReader(path);

        BufferedReader br= new BufferedReader(new FileReader(filename));
        String input;
        try{
            while ((input= br.readLine())!=null){
                String[] itemsPieces= input.split("\t");
                String shortDescription=itemsPieces[0];
                String details= itemsPieces[1];
                String stringDate= itemsPieces[2].trim();
                LocalDate date = LocalDate.parse(stringDate,formatter);
                ToDOItem item= new ToDOItem(shortDescription,details,date);
                todoItems.add(item);

            }
    }finally {
            if(br!=null){
                br.close();
            }
        }
        }

        public void storeToDoItem() throws IOException {
            //Path path= Paths.get(filename);
           // BufferedWriter bw= Files.newBufferedWriter(path);
            BufferedWriter bw= new BufferedWriter(new FileWriter(filename));
            try{
                Iterator<ToDOItem> iter= todoItems.iterator();
                while (iter.hasNext()){
                    ToDOItem item= iter.next();
                    bw.write(String.format("%s\t%s\t%s\n",item.getShortDescription(),item.getDetails()
                    ,item.getDeadLine().format(formatter)));

                }

            }finally {
                if(bw!=null){
                    bw.close();
                }
            }
        }
        public   void deleteItem( ToDOItem item){
        todoItems.remove(item);
        }
}
