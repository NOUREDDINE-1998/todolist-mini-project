module com.noureddine.todolist.todolist {
    requires javafx.controls;
    requires javafx.fxml;



    opens com.noureddine.todolist.todolist to javafx.fxml;
    exports com.noureddine.todolist.todolist;
}